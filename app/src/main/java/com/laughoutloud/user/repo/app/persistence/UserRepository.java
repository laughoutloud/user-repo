package com.laughoutloud.user.repo.app.persistence;

import com.laughoutloud.user.repo.core.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    public List<User> findByFirstName(String firstName);

    public List<User> findByLastName(String lastName);
}
