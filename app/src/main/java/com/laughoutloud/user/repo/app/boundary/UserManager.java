package com.laughoutloud.user.repo.app.boundary;

import com.laughoutloud.user.repo.app.model.UserListRepresentation;
import com.laughoutloud.user.repo.app.model.UserRepresentation;

import java.util.List;

public interface UserManager {

    /**
     * Add user
     */
    public void addUser(UserRepresentation userRepresentation);

    public void deleteUser();

    public void updateUser();

    public UserListRepresentation findAllUsers();

    public UserListRepresentation findAllUsersWithFirstName(String firstName);
}
