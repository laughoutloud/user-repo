package com.laughoutloud.user.repo.app.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.laughoutloud.user.repo.app")
public class AppConfig {
}
