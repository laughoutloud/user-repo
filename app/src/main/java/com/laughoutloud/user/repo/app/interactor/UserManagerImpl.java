package com.laughoutloud.user.repo.app.interactor;

import com.laughoutloud.user.repo.app.boundary.UserManager;
import com.laughoutloud.user.repo.app.model.RepresentationCreator;
import com.laughoutloud.user.repo.app.model.UserListRepresentation;
import com.laughoutloud.user.repo.app.model.UserRepresentation;
import com.laughoutloud.user.repo.app.persistence.UserRepository;
import com.laughoutloud.user.repo.core.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserManagerImpl implements UserManager {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void addUser(UserRepresentation userRepresentation) {
        User user = new User(userRepresentation.getFirstName(), userRepresentation.getLastName(), userRepresentation.getAttributes());
        userRepository.save(user);
    }

    @Override
    public void deleteUser() {

    }

    @Override
    public void updateUser() {

    }

    @Override
    public UserListRepresentation findAllUsers() {
        List<User> users = userRepository.findAll();
        UserListRepresentation result = RepresentationCreator.toRepresentation(users);
        return result;
    }

    @Override
    public UserListRepresentation findAllUsersWithFirstName(String firstName) {
        List<User> users = userRepository.findByFirstName(firstName);
        UserListRepresentation result = RepresentationCreator.toRepresentation(users);
        return result;
    }
}
