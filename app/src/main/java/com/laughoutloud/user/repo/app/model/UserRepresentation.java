package com.laughoutloud.user.repo.app.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Map;

public class UserRepresentation {

    @JsonProperty(required = true)
    @NotNull(message = "first name can't be null or empty")
    @Size(min = 1, max = 32, message = "first name has to be between 1 and 32 characters long")
    private String firstName;

    @JsonProperty(required = true)
    @NotNull(message = "last name can't be null or empty")
    @Size(min = 1, max = 32, message = "last name has to be between 1 and 32 characters long")
    private String lastName;

    private Map<String, String> attributes;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }
}
